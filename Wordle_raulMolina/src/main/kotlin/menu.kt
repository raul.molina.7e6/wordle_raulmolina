//Projecte Wordle
//Author: Raul Molina

import java.io.File
import java.util.*

fun main(){
    //COLORES
    val bgPURPLE = "\u001B[45m"
    val bgGREEN = "\u001B[42m"
    val bgYELLOW = "\u001B[43m"
    val bgWHITE = "\u001B[47m"
    val reset = "\u001B[0m"
    val WHITE = "\u001B[37m"
    val CYAN = "\u001B[36m"
    //LETRAS RESTANTES
    val letrasRestantes = mutableListOf<Char>('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p','q', 'r','s','t','u','v','w','x','y','z')

    val scanner = Scanner(System.`in`)

    var iniciar = false

    var partidas = 0
    var ganadas = 0

    //INICIO
    println("¡Bienvenido a Wordle!\n")
    while(true){
        println("\nEscribe 'start' para empezar, 'salir' para salir o 'tutorial' por si no sabes como jugar:")
        val inicio = scanner.next().lowercase()
        if(inicio == "start"){
            iniciar=true
            break
        }
        else if(inicio == "salir"){break}
        else if(inicio == "tutorial"){
            println("\nAdivina una palabra usando palabras de 5 letras\nEscribe como quieras: MAYUSCULAS, minusculas o AmBaS\nSolo tienes 6 intentos\nSi una letra es ${bgGREEN} verde $reset es porque se encuentra en la posición correcta\nSi es ${bgYELLOW} amarilla $reset es porque la contiene la palabra pero no en esa posición\nY si es ${bgWHITE} blanca $reset es porque no està\nNo puedes introducir una palabra que tenga letras repetidas")
        }
        else if(inicio == "puntuacion")
        else{
            println("No te he entendido, vuelve a probar:")}
    }

    while (iniciar){
        partidas++
        val palabrasJugadas = mutableListOf<String>()
        var acertado = false
        var contador = 6
        val palabras5 = File("C:\\Users\\rmoli\\Desktop\\prog\\ProyectoM03\\Wordle_raulMolina\\src\\main\\kotlin\\palabras\\palabras5").readText().toString().split(" ")
        val palabra = palabras5.random().lowercase()
        println("Prueba con una palabra:")
        while(true) {
            val entrada = scanner.next().lowercase()
            var history = "\n"
            var repetidas = false
            //COMPROBAR SI TIENE DOS LETRAS REPETIDAS
            for (i in 0..entrada.lastIndex) {
                for (j in i+1..entrada.lastIndex) {
                    if (entrada[i] == entrada[j]) {
                        repetidas=true
                        break
                    }
                }
            }
            if (entrada.lastIndex != 4) {
                print("\n$reset ¡No tiene 5 letras!\n")
            }
            else if(repetidas) print("\n¡Tu entrada no puede tener letras repetidas!\n")

            else if(entrada.uppercase() !in File("C:\\Users\\rmoli\\Desktop\\prog\\ProyectoM03\\Wordle_raulMolina\\src\\main\\kotlin\\palabras\\palabrasExistentes").readText().split(" ")){
                print("\n$reset¡Esa palabra no existe!\n")
            }
            else if(contador==1){
                break
            }
            else if(entrada.lowercase() == palabra.lowercase()){
                acertado=true
                break
            }else{

                for(i in 0..palabra.lastIndex){
                    if(entrada[i] == palabra[i]){
                        history+=(bgGREEN + entrada[i].uppercase()+ reset)
                    }else if(entrada[i] in palabra){
                        history+=(bgYELLOW + entrada[i].uppercase()+ reset)
                    }else{
                        history+=(bgWHITE + entrada[i].uppercase() + reset)
                        letrasRestantes-=entrada[i]
                    }
                }
                contador--
                print("\n$WHITE Intentos restantes: $contador $reset\n")
                print("\nLETRAS RESTANTES\n")
                for(i in 0..letrasRestantes.lastIndex){
                    print("${letrasRestantes[i].uppercase()} ")
                }
                println()
                palabrasJugadas.add(history)
                for(i in 0..palabrasJugadas.lastIndex){
                    print(palabrasJugadas[i])
                }
                println()
            }
        }
        if(acertado){
            println("\n${bgPURPLE}¡¡Has acertado!!$reset")
            ganadas++
        }else{
            print("\n$CYAN Has fallado :($reset\n")
            println("$WHITE La palabra era ${CYAN}${palabra.uppercase()}$reset")
        }

        //REPETIR JUEGO
        println("\nQuieres jugar otra vez? SI o NO")
        val repeat = scanner.next().lowercase()
        if(repeat=="no")break
    }
    println("¿Quieres ver tu puntuacion? SI o NO")
    val checkPuntuacion = scanner.next().lowercase()
    if(checkPuntuacion == "si"){
        println("Has ganado $ganadas de $partidas partidas jugadas")
    }
    println("\n¡¡Adios!!")
}