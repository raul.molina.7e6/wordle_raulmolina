import java.io.File
import java.util.*



fun main(){
    val bgPURPLE = "\u001B[45m"
    val bgGREEN = "\u001B[42m"
    val bgYELLOW = "\u001B[43m"
    val bgWHITE = "\u001B[47m"
    val bgCYAN = "\u001B[46m"
    val reset = "\u001B[0m"
    val ANSI_GREEN = "\u001B[32m"
    val ANSI_YELLOW = "\u001B[33m"
    val ANSI_WHITE = "\u001B[37m"
    val ANSI_PURPLE = "\u001B[35m"
    val ANSI_CYAN = "\u001B[36m"
    val scanner = Scanner(System.`in`)
    var iniciarTablero = false



    if(iniciarTablero){
        val palabrasJugadas = mutableListOf<String>()
        var acertado = false
        var contador = 6
        val palabras5 = File("C:\\Users\\rmoli\\Desktop\\prog\\ProyectoM03\\Wordle_raulMolina\\src\\main\\kotlin\\palabras\\palabras5").readText().toString().split(" ")
        val palabra = palabras5.random().lowercase()
        println(palabra)
        println("Prueba con una palabra:")
        print("$ANSI_WHITE Intentos restantes: $contador\n")
        while(true) {
            val entrada = scanner.nextLine().lowercase()
            var history = "\n"
            var repetidas = false
            //COMPROBAR SI TIENE DOS LETRAS REPETIDAS
            for (i in 0..entrada.lastIndex) {
                for (j in i+1..entrada.lastIndex) {
                    if (entrada[i] == entrada[j]) {
                        repetidas=true
                        break
                    }
                }
            }
            if (entrada.lastIndex != 4) {
                println("$reset ¡No tiene 5 letras!")
            }
            else if(repetidas) println("¡Tu entrada no puede tener letras repetidas!")

            else if(palabra.uppercase() !in File("C:\\Users\\rmoli\\Desktop\\prog\\ProyectoM03\\Wordle_raulMolina\\src\\main\\kotlin\\palabras\\palabrasExistentes").readText().split(" ")){
                println("¡Esa palabra no existe!")
            }
            else if(contador==1){
                break
            }
            else if(entrada.lowercase() == palabra.lowercase()){
                acertado=true
                break
            }else{

                for(i in 0..palabra.lastIndex){
                    if(entrada[i] == palabra[i]){
                        history+=(bgGREEN + entrada[i]+ reset)
                    }else if(entrada[i] in palabra){
                        history+=(bgYELLOW + entrada[i]+ reset)
                    }else{
                        history+=(bgWHITE + entrada[i] + reset)
                    }
                }
                contador--
            }
            print("\n$ANSI_WHITE Intentos restantes: $contador $reset")
            palabrasJugadas.add(history)
            for(i in 0..palabrasJugadas.lastIndex){
                print(palabrasJugadas[i])
            }
            println()
        }
        if(acertado){
            println("${bgPURPLE}¡¡Has acertado!!$reset")
        }else{
            print("$ANSI_CYAN Has fallado :($reset\n")
            println("$ANSI_WHITE La palabra era ${ANSI_CYAN}${palabra.uppercase()}$reset")
        }
    }

}